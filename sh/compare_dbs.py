import os,sys,argparse
import json
sys.path.insert(1, os.path.abspath(os.path.dirname(__file__)))
import time
import ci_shell

HOST = '69.204.87.237'
PORT = 54522

query1 = 'SELECT sum(person_count) pc, room_id, rooms.room_name FROM roomproperties,rooms where rooms.id = roomproperties.room_id group by room_id having pc = 0;'
query2 = 'SELECT room_name, max(date_time), temperature FROM roomproperties,rooms WHERE rooms.id = roomproperties.room_id GROUP BY room_id ORDER BY temperature DESC'
count = 'select count(*) from roomproperties;'

with open('compare.txt', 'wb') as f:
	while True:
		c = [json.loads(x) for x in ci_shell.query(count, HOST, PORT)]
		q1 = [json.loads(x) for x in ci_shell.query(query1, HOST, PORT)]
		q2 = [json.loads(x) for x in ci_shell.query(query2, HOST, PORT)]
		d = {'count' : c, 'q1' : q1, 'q2' : q2}
		f.write(json.dumps(d) + '\n')
		f.flush()
		time.sleep(5)

