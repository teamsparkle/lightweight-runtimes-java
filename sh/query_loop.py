import os,sys
sys.path.insert(1, os.path.abspath(os.path.dirname(__file__)))
import argparse
import ci_shell
import time
import requests
from random import randint

def start_query_loop(runner, interval):
   while(True):
       query = """select count(*) from roomproperties"""
       runner.do_query(query)
       print("*****")
       time.sleep(interval)

def insert_random(session=None):
    if not session:
        session = requests.Session()
    queryString = {
        'queryType': 'insert',
        'room': randint(1,5),
        'temperature': randint(20, 40),
        'occupants': randint(3, 100),
        'timestamp': int(time.time())
    }
    response = session.get("http://localhost:54522/query", params=queryString)
    msg = "inserted %s with response %s" % (queryString, response)
    print msg
    print "_" * len(msg)


if __name__=='__main__':
    parser = argparse.ArgumentParser(description="Run a query in a loop")
    parser.add_argument('-i', '--interval', type=int, help="Interval between queries")
    parser.add_argument('-t', '--type', type=str, default="query", help="Query operation to perform")
    args = parser.parse_args()
    if args.type.lower().startswith("q"):
        runner = ci_shell.QueryCmdline(None)
        runner.HOST = 'localhost'
        runner.PORT = 54522
        start_query_loop(runner, args.interval)
    elif args.type.lower().startswith("i"):
        session = requests.Session()
        while(True):
            insert_random(session)
            time.sleep(args.interval)
    else:
        print "Operation %s not found" % args.type