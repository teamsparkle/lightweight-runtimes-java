#!/usr/bin/python
import os,sys,argparse
import logging
import json
import shlex

from pycommons import pycommons
from pycommons import generic_logging
logger = logging.getLogger()

import common
import run

MAX_HEAP = 250 * 1024
MIN_HEAP = 1 * 1024

def do_binary_search(max_heap, min_heap, args):
	if max_heap == min_heap:
		return min_heap
	mid = (max_heap + min_heap) / 2
	cur_mid = mid
	java_opts = '-Xms%dk -Xmx%dk' % (mid, mid)

	cmdline = run.generate_cmdline(args, java_opts)

	ret, stdout, stderr = run.run(cmdline)
	for line in stdout.split('\n'):
		logger.info(line)
		try:
			obj = json.loads(line)
			d = {'heapSize' : mid}
			d.update(obj)
			logger.info(json.dumps(d))
		except:
			continue
	if ret != 0 and not 'java.lang.OutOfMemoryError:' in stdout:
		raise Exception('Failed with an error other than java.lang.OutOfMemoryError! Terminating ...')


	if ret != 0:
		min_heap = mid
	else:
		max_heap = mid

	new_mid = (max_heap + min_heap) / 2
	if cur_mid == new_mid:
		if min_heap == mid:
			return max_heap
		elif max_heap == mid:
			return min_heap

	return do_binary_search(max_heap, min_heap, args)

def find_min_heap(args):
	logger.debug('args: %s' % (args))
	min_heap = do_binary_search(MAX_HEAP, MIN_HEAP, args)

	logger.info("Min heap: %d" % (min_heap))

	return min_heap

def main(argv):
	parser = common.setup_parser()
	args = parser.parse_args(argv[1:])

	common.build_args(args)

	global logger
	generic_logging.init(args.level, args.log)
	logger = logging.getLogger()

	find_min_heap(args.args)

if __name__ == '__main__':
	main(sys.argv)
