
use 100rows;

DROP TABLE  IF EXISTS NATION;
CREATE TABLE NATION ( NATIONKEY INTEGER NOT NULL,  
NAME CHAR(25) NOT NULL,  
REGIONKEY INTEGER NOT NULL,  
COMMENT VARCHAR(152),
PRIMARY KEY (NATIONKEY)
);  

DROP TABLE  IF EXISTS REGION;
CREATE TABLE REGION ( REGIONKEY INTEGER NOT NULL,  
NAME CHAR(25) NOT NULL,  
COMMENT VARCHAR(152),
PRIMARY KEY (REGIONKEY)
);  

DROP TABLE  IF EXISTS PART;
CREATE TABLE PART ( PARTKEY INTEGER NOT NULL,  
NAME VARCHAR(55) NOT NULL,  
MFGR CHAR(25) NOT NULL,  
BRAND CHAR(10) NOT NULL,  
TYPE VARCHAR(25) NOT NULL,  
SIZE INTEGER NOT NULL,  
CONTAINER CHAR(10) NOT NULL,  
RETAILPRICE DECIMAL(15,2) NOT NULL,  
COMMENT VARCHAR(23) NOT NULL ,
PRIMARY KEY (PARTKEY)


);  

DROP TABLE  IF EXISTS SUPPLIER;
CREATE TABLE SUPPLIER ( SUPPKEY INTEGER NOT NULL,  
NAME CHAR(25) NOT NULL,  
ADDRESS VARCHAR(40) NOT NULL,  
NATIONKEY INTEGER NOT NULL,  
PHONE CHAR(15) NOT NULL,  
ACCTBAL DECIMAL(15,2) NOT NULL,  
COMMENT VARCHAR(101) NOT NULL,
PRIMARY KEY (SUPPKEY))

; 
 
DROP TABLE  IF EXISTS PARTSUPP; 
CREATE TABLE PARTSUPP ( PARTKEY INTEGER NOT NULL,  
SUPPKEY INTEGER NOT NULL,  
AVAILQTY INTEGER NOT NULL,  
SUPPLYCOST DECIMAL(15,2) NOT NULL,  
COMMENT VARCHAR(199) NOT NULL 


);  

DROP TABLE  IF EXISTS CUSTOMER;
CREATE TABLE CUSTOMER ( CUSTKEY INTEGER NOT NULL,  
NAME VARCHAR(25) NOT NULL,  
ADDRESS VARCHAR(40) NOT NULL,  
NATIONKEY INTEGER NOT NULL,  
PHONE CHAR(15) NOT NULL,  
ACCTBAL DECIMAL(15,2) NOT NULL,  
MKTSEGMENT CHAR(10) NOT NULL,  
COMMENT VARCHAR(117) NOT NULL,
PRIMARY KEY (CUSTKEY)

);
  
  DROP TABLE  IF EXISTS ORDERS;
CREATE TABLE ORDERS ( ORDERKEY INTEGER NOT NULL,  
CUSTKEY INTEGER NOT NULL,  
ORDERSTATUS CHAR(1) NOT NULL,  
TOTALPRICE DECIMAL(15,2) NOT NULL,  
ORDERDATE DATE NOT NULL,  
ORDERPRIORITY CHAR(15) NOT NULL,  
CLERK CHAR(15) NOT NULL,  
SHIPPRIORITY INTEGER NOT NULL,  
COMMENT VARCHAR(79) NOT NULL,
PRIMARY KEY (ORDERKEY)

);  

DROP TABLE  IF EXISTS LINEITEM;
CREATE TABLE LINEITEM ( ORDERKEY INTEGER NOT NULL,  
PARTKEY INTEGER NOT NULL,  
SUPPKEY INTEGER NOT NULL,  
LINENUMBER INTEGER NOT NULL,  
QUANTITY DECIMAL(15,2) NOT NULL,  
EXTENDEDPRICE DECIMAL(15,2) NOT NULL,  
DISCOUNT DECIMAL(15,2) NOT NULL,  
TAX DECIMAL(15,2) NOT NULL,  
RETURNFLAG CHAR(1) NOT NULL,  
LINESTATUS CHAR(1) NOT NULL,  
SHIPDATE DATE NOT NULL,  
COMMITDATE DATE NOT NULL,  
RECEIPTDATE DATE NOT NULL,  
SHIPINSTRUCT CHAR(25) NOT NULL,  
SHIPMODE CHAR(10) NOT NULL,  
COMMENT VARCHAR(44) NOT NULL

);  






  
   
   load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\lineitem.tbl'
into table LINEITEM fields terminated by '|'
 enclosed by '|'
 lines terminated by '\n'
   (orderkey  ,    
partkey    ,   
suppkey    ,   
linenumber  ,  
quantity     , 
extendedprice ,
discount      ,
tax           ,
returnflag    ,
linestatus    ,
shipdate      ,
commitdate    ,
receiptdate   ,
shipinstruct  ,
shipmode   ,   
comment     ) ;
     
     
load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\customer.tbl'
 into table CUSTOMER fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
 (custkey   , 
name     ,  
address    ,
nationkey  ,
phone      ,
acctbal    ,
mktsegment ,
comment    );
        
load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\orders.tbl'
 into table ORDERS fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
  (
orderkey   ,  
custkey      ,
orderstatus , 
totalprice   ,
orderdate   , 
orderpriority,
clerk  ,      
shippriority ,
comment   )  ;
   
   
   load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\NATION.tbl'
 into table NATION fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
  ( NATIONKEY ,
NAME ,
REGIONKEY ,
COMMENT );
  
  
  
  
  
  load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\REGION.tbl'
 into table REGION fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
  (
  REGIONKEY ,
NAME ,
COMMENT
)  ;
  
  
  
  
  
  load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\PART.tbl'
 into table PART fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
  (NAME ,  
MFGR ,  
BRAND ,  
TYPE ,
SIZE ,
CONTAINER ,
RETAILPRICE ,
COMMENT );

  
  
  
  
  
  load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\SUPPLIER.tbl'
 into table SUPPLIER fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
  (
   SUPPKEY ,
NAME ,
ADDRESS ,
NATIONKEY ,
PHONE ,
ACCTBAL ,
COMMENT );
  
  
  
  load data local infile 
'C:\\Users\\Sathish\\Dropbox\\UB_Spring_2014\\DB\\share\\10mb\\PARTSUPP.tbl'
 into table PARTSUPP fields terminated by '|'
  enclosed by '|'
  lines terminated by '\n'
  (
  PARTKEY ,
SUPPKEY ,
AVAILQTY ,
SUPPLYCOST ,
COMMENT  );  

  -- ALTER TABLE LINEITEM ADD INDEX (orderkey,commitdate,shipdate,receiptdate,shipmode)
-- ALTER TABLE ORDERS ADD INDEX (orderkey, orderpriority);
   

