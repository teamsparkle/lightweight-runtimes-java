CREATE TABLE ROOMPROPERTIES ( 
	id           INT,
	room_id       INT,
	temperature  DECIMAL,
	person_count INT,
	date_time    INT       
);

CREATE TABLE ROOMS(	
	id INT,
	room_name VARCHAR(50)	
);