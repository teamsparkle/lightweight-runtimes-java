SELECT nation.name, sum(lineitem.extendedprice * (1 - lineitem.discount)) AS revenue 
FROM region, nation, customer, orders, lineitem, supplier
 WHERE customer.custkey = orders.custkey 
 AND lineitem.orderkey = orders.orderkey 
 AND lineitem.suppkey = supplier.suppkey 
 AND customer.nationkey = nation.nationkey 
 AND supplier.nationkey = nation.nationkey 
 AND nation.regionkey = region.regionkey 
 AND region.name = 'MIDDLE EAST'
 AND orders.orderdate >= DATE('1996-01-01')
 AND orders.orderdate < DATE('1997-01-01') 
 GROUP BY nation.name 
 ORDER BY revenue DESC