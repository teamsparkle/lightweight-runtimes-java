CREATE TABLE LINEITEM (
        orderkey       INT,
        partkey        INT,
        suppkey        INT,
        linenumber     INT,
        quantity       DECIMAL,
        extendedprice  DECIMAL,
        discount       DECIMAL,
        tax            DECIMAL,
        returnflag     CHAR(1),
        linestatus     CHAR(1),
        shipdate       DATE,
        commitdate     DATE,
        receiptdate    DATE,
        shipinstruct   CHAR(25),
        shipmode       CHAR(10),
        comment        VARCHAR(44)
    )


CREATE TABLE ORDERS (
        orderkey       INT,
        custkey        INT,
        orderstatus    CHAR(1),
        totalprice     DECIMAL,
        orderdate      DATE,
        orderpriority  CHAR(15),
        clerk          CHAR(15),
        shippriority   INT,
        comment        VARCHAR(79)
    )

CREATE TABLE PART (
        partkey      INT,
        name         VARCHAR(55),
        mfgr         CHAR(25),
        brand        CHAR(10),
        type         VARCHAR(25),
        size         INT,
        container    CHAR(10),
        retailprice  DECIMAL,
        comment      VARCHAR(23)
    )


CREATE TABLE CUSTOMER (
        custkey      INT,
        name         VARCHAR(25),
        address      VARCHAR(40),
        nationkey    INT,
        phone        CHAR(15),
        acctbal      DECIMAL,
        mktsegment   CHAR(10),
        comment      VARCHAR(117)
    )

CREATE TABLE SUPPLIER (
        suppkey      INT,
        name         CHAR(25),
        address      VARCHAR(40),
        nationkey    INT,
        phone        CHAR(15),
        acctbal      DECIMAL,
        comment      VARCHAR(101)
    )

CREATE TABLE PARTSUPP (
        partkey      INT,
        suppkey      INT,
        availqty     INT,
        supplycost   DECIMAL,
        comment      VARCHAR(199)
    )

CREATE TABLE NATION (
        nationkey    INT,
        name         CHAR(25),
        regionkey    INT,
        comment      VARCHAR(152)
    )

CREATE TABLE REGION (
        regionkey    INT,
        name         CHAR(25),
        comment      VARCHAR(152)
    );
    
SELECT supp_nation, cust_nation, l_year, SUM(volume) as revenue
FROM (
  SELECT n1.name AS supp_nation,
         n2.name AS cust_nation,
         (DATE_PART('year', l.shipdate)) AS l_year,
         l.extendedprice * (1 - l.discount) AS volume
  FROM supplier s, lineitem l, orders o, customer c, nation n1, nation n2
  WHERE s.suppkey = l.suppkey
    AND o.orderkey = l.orderkey
    AND c.custkey = o.custkey
    AND s.nationkey = n1.nationkey 
    AND c.nationkey = n2.nationkey 
    AND (
      (n1.name = 'FRANCE' and n2.name = 'GERMANY') 
        OR
      (n1.name = 'GERMANY' and n2.name = 'FRANCE')
    )
    AND (l.shipdate BETWEEN DATE('1995-01-01') AND DATE('1996-12-31') )
  ) AS shipping
GROUP BY supp_nation, cust_nation, l_year;