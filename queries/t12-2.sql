select
	lineitem.shipmode,
	sum(case
		when orders.orderpriority = '1-URGENT'
			or orders.orderpriority = '2-HIGH'
			then 1
		else 0
	end) as high_line_count,
	sum(case
		when orders.orderpriority <> '1-URGENT'
			and orders.orderpriority <> '2-HIGH'
			then 1
		else 0
	end) as low_line_count
from
	orders,
	lineitem
where
	orders.orderkey = lineitem.orderkey
	and lineitem.shipmode in ('X', 'Y')
	and lineitem.commitdate < lineitem.receiptdate
	and lineitem.shipdate < lineitem.commitdate
	and lineitem.receiptdate >= DATE('1985-03-15')
	and lineitem.receiptdate < DATE('1995-03-15')
group by
	lineitem.shipmode
order by
	lineitem.shipmode;