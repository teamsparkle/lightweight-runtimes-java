import argparse

from pycommons import pycommons

def setup_parser():
	parser = argparse.ArgumentParser()

	parser.add_argument('--data', type=str, default='dat/10mb', action='store')
	parser.add_argument('--query', type=str, default='t1')
	parser.add_argument('--repeat', type=int, default=1)
	parser.add_argument('--args', type=str,
			default='--data {DATA} queries/{QUERY}.sql --db bdb --repeat {REPEAT}',
			help='Program arguments')
	parser.add_argument('--log', type=str, default=None,
			help='Log to file (default is STDOUT)')

	parser.add_argument('--level', action=pycommons.LoggingLevelAction,
			type=str, default='DEBUG', help='Logging level')

	return parser

def build_args(args, query=None, repeat=None, data=None):
	if not query:
		query = args.query
	if not repeat:
		repeat = args.repeat
	if not data:
		data = args.data
	args.args = args.args.replace('{DATA}', data)
	args.args = args.args.replace('{QUERY}', query)
	args.args = args.args.replace('{REPEAT}', str(repeat))
