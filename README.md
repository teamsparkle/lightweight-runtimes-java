# README #


### CSE 662 ###

* Lightweight in-memory database for streaming data on IoT devices
* 2.0

### How do I get set up? ###

* Install Java 8 and make sure it's the default
* Install ant
* In the repository directory, run "ant"
* Start the database ". sh/sparkledb_java_server"


### Who do I talk to? ###

* dhineshns@buffalo.edu
* guruprasad@buffalo.edu
* oobrutu@buffalo.edu
* shivamar@buffalo.edu