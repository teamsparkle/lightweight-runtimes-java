
    
SELECT customer.custkey, 
sum(lineitem.extendedprice * (1 - lineitem.discount)) AS revenue,
 customer.acctbal,
 nation.name, 
 customer.address, 
 customer.phone, 
 customer.comment 
FROM customer, orders, lineitem, nation 
WHERE customer.custkey = orders.custkey 
AND lineitem.orderkey = orders.orderkey 
AND orders.orderdate >= date('1993-09-01')
 AND orders.orderdate < date('1993-12-01') 
 AND lineitem.returnflag = 'R'
 AND customer.nationkey = nation.nationkey
 GROUP BY customer.custkey, customer.acctbal, customer.phone, nation.name, customer.address, customer.comment 
 ORDER BY revenue
 LIMIT 20;