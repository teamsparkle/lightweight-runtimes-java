import os;
import sys;
import time;

import argparse
import re
import json

parser = argparse.ArgumentParser()
parser.add_argument("-ds", "--dataset", type=int,
                    help="specify dataset ie., 10 0r 20 0r 30...")
parser.add_argument("-di", "--dont_insert", action="store_true",
                    help="specify if you want to run the queries without inserting data into db again")
parser.add_argument("-pl", "--plot", action="store_true",
                    help="specify if you want to plot the graph")

args = parser.parse_args()
load_sqlite = open("load_sqlite.sql", "r+");
load_sqlite_content = load_sqlite.read();
load_sqlite.close();
load_sqlite_content = re.sub(r'.{2}mb',str(args.dataset)+r'mb', load_sqlite_content)
load_sqlite = open("load_sqlite.sql", "w+");
load_sqlite.write(load_sqlite_content);
load_sqlite.close();


tpch_folder = "tpch_queries";
# all the sqlite commands
load_command = " \'.read load_sqlite.sql \' "
echo_off = " \'.echo off \' "

# shell commands to run the sqlite commands
os.system("../sqlite3/sqlite3 ex1 " + echo_off );

if(not args.dont_insert):
	os.system("../sqlite3/sqlite3 ex1 " + load_command);



runtime_for_queries = {};
for file in os.listdir(tpch_folder):
	if file.endswith(".sql"):
		file_handle = open(os.path.join( tpch_folder, file), "r+");
		file_content = file_handle	.read();
		file_content = file_content.replace("\n", " ");
		file_content = "\"" + file_content + "\"";
		print("------------------------------"+file+"-------------------------------");
		
		#timing
		start = time.time()
		os.system("../sqlite3/sqlite3 ex1 " + file_content);
		stop= time.time()
		runtime_for_queries[file] = stop-start;


for query, time in runtime_for_queries.iteritems():
	print str(query) + "---->" + str(time);

result = {};
result[args.dataset] = runtime_for_queries;

output_file = open("output.json", "w+"); 
output_file.write(json.dumps(result));
output_file.close();

if( args.plot):
	import matplotlib.pyplot as plt


	plt.bar(range(len(runtime_for_queries)), runtime_for_queries.values(), align='center');
	plt.xticks(range(len(runtime_for_queries)), runtime_for_queries.keys())

	plt.ylabel('Runtime (s)')
	plt.xlabel('TPCH Queries')

	plt.show()

