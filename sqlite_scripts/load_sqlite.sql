drop table LINEITEM;
drop table ORDERS;
drop table CUSTOMER;
drop table PART;
drop table REGION;
drop table NATION;
drop table SUPPLIER;
CREATE TABLE LINEITEM (
            orderkey       INT,
            partkey        INT,
            suppkey        INT,
            line10mber     INT,
            quantity       DECIMAL,
            extendedprice  DECIMAL,
            discount       DECIMAL,
            tax            DECIMAL,
            returnflag     CHAR(1),
            linestatus     CHAR(1),
            shipdate       DATE,
            commitdate     DATE, 
            receiptdate    DATE,
            shipinstruct   CHAR(25),
            shipmode       CHAR(10),
            comment        VARCHAR(44)
        );
.separator "|"
.import ../dat/10mb/lineitem.tbl LINEITEM


CREATE TABLE ORDERS (
        orderkey       INT,
        custkey       INT,
        orderstatus    CHAR(1),
        totalprice     DECIMAL,
        orderdate      DATE,
        orderpriority  CHAR(15),
        clerk          CHAR(15),
        shippriority   INT,
        comment        VARCHAR(79)
    );
.separator "|"
.import ../dat/10mb/orders.tbl ORDERS

CREATE TABLE CUSTOMER (
        custkey      INT,
        name         VARCHAR(25),
        address      VARCHAR(40),
        nationkey    INT,
        phone        CHAR(15),
        acctbal      DECIMAL,
        mktsegment   CHAR(10),
        comment      VARCHAR(117)
    );
.separator "|"
.import ../dat/10mb/customer.tbl CUSTOMER

CREATE TABLE PART (
        partkey      INT,
        name         VARCHAR(55),
        mfgr         CHAR(25),
        brand        CHAR(10),
        type         VARCHAR(25),
        size         INT,
        container    CHAR(10),
        retailprice  DECIMAL,
        comment      VARCHAR(23)
    );
.separator "|"
.import ../dat/10mb/part.tbl PART


CREATE TABLE SUPPLIER (
        suppkey      INT,
        name         CHAR(25),
        address      VARCHAR(40),
        nationkey    INT,
        phone        CHAR(15),
        acctbal      DECIMAL,
        comment      VARCHAR(101)
    );
.separator "|"
.import ../dat/10mb/supplier.tbl SUPPLIER

CREATE TABLE NATION (
        nationkey    INT,
        name         CHAR(25),
        regionkey    INT,
        comment      VARCHAR(152)
    );
.separator "|"
.import ../dat/10mb/nation.tbl NATION

CREATE TABLE REGION (
        regionkey    INT,
        name         CHAR(25),
        comment      VARCHAR(152)
    );
.separator "|"
.import ../dat/10mb/region.tbl REGION
