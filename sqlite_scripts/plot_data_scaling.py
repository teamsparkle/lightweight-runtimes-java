'''
{
   "10":{  
      "t3.sql":42.20164108276367
   }, 
   "20":{  
      "t3.sql":427.33550202846527
   },
   "30":{  
      "t3.sql":938.2765380144119
   },
   "40":{  
      "t3.sql":1678.4801100492477
   }, 
   "50":{  
      "t3.sql":2571.886137008667
   },
   "60":{  
      "t3.sql":3764.9552949666977
   },
   "70":{  
      "t3.sql":4956.165601968765
   }, 
   "80":{  
      "t3.sql":6614.97682094574
   }
}

'''

import os,sys,argparse

FILE_DIR = os.path.abspath(os.path.dirname(__file__))
BASE_DIR = os.path.join(FILE_DIR, '..')
sys.path.insert(1, BASE_DIR)

import plot
import json

def process(file):
	d = json.loads(open(file).read())

	for key in d.keys():
		value = d[key]
		new_value = {}
		new_value['timeTaken'] = []
		new_value['timeTaken'].append(value)
		d[key] = new_value
	return d

def main(argv):
	parser = argparse.ArgumentParser()
	parser.add_argument('file')

	args = parser.parse_args(sys.argv[1:])

	d = process(args.file)
	plot.data_scaling(d, prefix='sqlite')

if __name__ == '__main__':
	main(sys.argv)
