import os
import sys
import argparse
import json
from heapq import heappush, nlargest

"""
This is a command line utility for extracting method names 
and their active CPU percentages from a java hprof dump, and dumps 
the result a json file which can be then be used for further 
performance analysis or visualization
"""

class HprofProcessor(object):
	def __init__(self, hprof_input):
		if not os.path.exists(hprof_input):
			raise Exception("File %s does not exist" % hprof_input)
		self.hprof_input = hprof_input

	def process_hprof(self):
		"""
		Returns a dictionary of <method_name, percentage_cpu_time>,
		along with the total of all CPU percentages recorded
		by the profiler
		"""
		elements = {}
		duplicates = {}
		total = 0.0
		begin_next = False
		with open(self.hprof_input) as inp:
			for line in inp:
				if not begin_next:
					# Identifies start of actual profiling stats
					if "CPU SAMPLES BEGIN" in line:
						begin_next = True
				elif "CPU SAMPLES END" in line:
					break
				if begin_next:			
					dat = line.split()
					if dat[0].isdigit():
						percent = float(dat[1].replace("%", ""))
						key = dat[5]

						# Handle duplicate method entries
						if key in elements:
							key = "%s (%s)" % (key, duplicates.get(key, 0) + 1)
						elements[key] = percent
						total += percent
		return elements, total

	def get_top_k(self, k):
		"""
		Given a dictionary of hprof entries, returns a dictionary containing only 
		methods with the top k CPU active times, with the rest aggregated under
		the key 'others (<others_count>)'
		"""
		if k < 1:
			return {}
		hprof_dict = self.process_hprof()[0]
		heap = []
		new_dict = {}
		others_count = 0
		topk = nlargest(k, hprof_dict.values())
		for k,v in hprof_dict.items():
			if v in topk:
				new_dict[k] = v
			else:
				others = new_dict.get('others', 0.0)
				new_dict['others'] = others + v
				others_count += 1
		# Add this entry's percentage to the current value of 'others'
		new_dict["others (%s)" % others_count] = new_dict.pop("others", 0)
		return new_dict

	def dump_full_json(self, json_file):
		with open(args.output_file, "w") as out:
			json.dump(self.process_hprof()[0], fp=out, indent=4)

	def dump_topk_json(self, json_file, k):
		with open(args.output_file, "w") as out:
			json.dump(self.get_top_k(k), fp=out, indent=4)

if __name__=="__main__":
	parser = argparse.ArgumentParser(description="process a hprof file")
	parser.add_argument("hprof_file", help="the hprof file to be processed")
	parser.add_argument("output_file", help="the output file to be dumped")

	args = parser.parse_args()
	hprof_processor = HprofProcessor(args.hprof_file)
	hprof_processor.dump_topk_json(args.output_file, 20)
