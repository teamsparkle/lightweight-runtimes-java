import logging
from pycommons import generic_logging
generic_logging.init(level=logging.DEBUG)
logger = logging.getLogger('combined_plots')

import os,sys,argparse
import re
import json
import itertools

import plot

sizes = [10, 40]
queries = ['t1', 't3', 't6', 't10']

overall_data = []
for q in queries:
	lines = open('plots/' + q + '_extracted', 'rb').readlines()
	assert len(lines) == len(sizes)

	values = []
	for s, l in itertools.izip(sizes, lines):
		d = {}
		d['size'] = s
		d['data'] = json.loads(l)
		values.append(d)

	d = {'query' : q, 'values' : values}
	overall_data.append(d)


for pt in overall_data:
	# Each pt is 1 plot
	fig = plot.new_fig()
	ax = fig.add_subplot(111)

	q = pt['query']
	query = pt['query'].replace('t', 'TPC-H')
	values = pt['values']

	print q
	for v in values:
		size = v['size']
		data = v['data']
		memory = sorted([int(x) for x in data.keys()])
		performance = [data[str(x)] for x in memory]

		print memory
		print performance
		plot.memory_performance(memory, performance, ax=ax, label='%d MB' % (size))

	print
	filename = '%s_varying_datasets.pdf' % (q)
	ax.legend()
	plot.save(ax, xlabel='Memory (KB)', ylabel='Performance (s)',
			title='%s Memory vs Performance' % (query), filename=filename)
