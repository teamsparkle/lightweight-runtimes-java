#!/usr/bin/python

import os,sys,argparse
import glob

from pycommons import pycommons
from pycommons import generic_logging
import logging
logger = logging.getLogger()

import common
import run

def setup_parser(parser=None):
	if not parser:
		parser = argparse.ArgumentParser()
	
	parser.add_argument('--queries', '-q', action=pycommons.ListAction,
			type=str, required=True, help='Queries to run')
	parser.add_argument('--datasets', '-d', action=pycommons.ListAction,
			type=str, required=True, help='Queries to run')

	return parser
def main(argv):
	parser = common.setup_parser()
	parser = setup_parser()
	args = parser.parse_args(argv[1:])

	global logger
	generic_logging.init(level=args.level)
	logger = logging.getLogger()

	for q in args.queries:
		for d in args.datasets:
			common.build_args(args, query=q, data=d)
			cmdline = generate_cmdline(args.args, args.java_opts)
			logger.info(cmdline)
			returncode, stdout, stderr = pycommons.run(cmdline, log=True, fail_on_error=True)

if __name__ == '__main__':
	main(sys.argv)

