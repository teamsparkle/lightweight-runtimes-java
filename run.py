#!/usr/bin/python

import os,sys,argparse
import glob

from pycommons import pycommons
from pycommons import generic_logging
import logging
logger = logging.getLogger()

import common

BASEDIR = os.path.abspath(os.path.dirname(__file__))
LIBS = ':'.join(glob.glob(os.path.join(BASEDIR, 'lib', '*.jar')))

def setup_parser(parser=None):
	if not parser:
		parser = argparse.ArgumentParser()

	parser.add_argument('--java-opts', '-j', type=str, default='',
			help='Java arguments')

	return parser

def run(cmdline, log=False, fail_on_error=True):
	ret = 1
	stdout = None
	stderr = None
	ret, stdout, stderr = pycommons.run(cmdline, log=log, fail_on_error=False)
	if 'exceptions' in stdout:
		ret = 1

	return ret, stdout, stderr

def generate_cmdline(args, java_opts=None):
	cmdline = ['java']
	if java_opts:
		for arg in java_opts.split(' '):
			cmdline.append(arg)
	cmdline.append('-cp %s/build:%s edu.buffalo.cse562.Main %s' % (BASEDIR, LIBS, args))

	cmdline = ' '.join(cmdline)

	return cmdline

def main(argv):
	parser = common.setup_parser()
	parser = setup_parser(parser)
	args = parser.parse_args(argv[1:])

	global logger
	generic_logging.init(level=args.level)
	logger = logging.getLogger()

	common.build_args(args)
	cmdline = generate_cmdline(args.args, args.java_opts)
	logger.info(cmdline)
	returncode, stdout, stderr = pycommons.run(cmdline, log=True, fail_on_error=True)

if __name__ == '__main__':
	main(sys.argv)

