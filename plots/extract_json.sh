#!/bin/bash

SIZES="10 40"
QUERIES="t1 t3 t6 t10"

for q in $QUERIES; do
	rm -rf ${q}_extracted
	for s in $SIZES; do
		tail -n 4 ${q}_${s}_mem_perf | head -n1 >> ${q}_extracted
	done
done
