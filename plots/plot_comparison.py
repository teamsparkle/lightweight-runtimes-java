import os,sys,argparse
import json
import matplotlib.pyplot as plt

xaxis = []
sparkledb_times = []
sqlite_times = []

for query in ['q1', 'q2']:
	with open('compare.txt', 'rb') as f:
		for line in f:
			j = json.loads(line)
			count = int(j['count'][0]['response'].strip())
			sparkledb = j[query][0]['time_taken_ms']
			sqlite = j[query][1]['time_taken_ms']

			xaxis.append(count)
			sparkledb_times.append(sparkledb)
			sqlite_times.append(sqlite)

	import pdb
	pdb.set_trace()
	plt.xscale('log')
	plt.plot(xaxis, sparkledb_times, label='SparkleDB', color='b')
	plt.plot(xaxis, sqlite_times, label='SQLite', color='g')
	plt.savefig(query + '.pdf')
