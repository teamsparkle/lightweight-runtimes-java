import json
import plot

import matplotlib.pyplot as plt

fig = plot.new_fig()
ax = fig.add_subplot(111)

d = {}

with open('data_scaling.json', 'rb') as f:
	for line in f:
		line = line.strip()
		if line == '':
			continue
		try:
			j = json.loads(line)
		except:
			print 'Failed!\n%s\n' % (line)
			continue
		for key in j.keys():
			assert d.get(key, None) is None
			d[key] = j[key]

plot.data_scaling(d, ax, label='SparkleDB')
import sqlite_scripts.plot_data_scaling as sqlite_plot
d = sqlite_plot.process('sqlite_scripts/data_scaling.json')
plot.data_scaling(d, ax, label='SQLite', save=True)

