import os,sys,argparse
import json
import operator
import numpy as np

import plot
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.axes_divider import make_axes_area_auto_adjustable
import pylab

def process(file):
	d = json.loads(open(file).read())
	return d

def plot_hprof(d):
	real_y_axis = [x[0] for x in sorted(d.items(), key=operator.itemgetter(1))]
	# LATEX Sanitize
	sanitized_y_axis = [x.replace('$', r'\$').replace('_', r'\_') for x in real_y_axis]
	y_axis_pos = np.arange(len(real_y_axis))	# For centering
	x_axis = [d[x] for x in real_y_axis]

	fig = plt.figure(figsize=(6.6,5))
	ax = fig.add_subplot(111)

	ax.barh(y_axis_pos, x_axis, align='center', edgecolor=None, linewidth=0)
	ax.set_yticks(y_axis_pos)
	ax.set_yticklabels(sanitized_y_axis)
	make_axes_area_auto_adjustable(ax, pad=0.1, use_axes=[ax])
	ax.set_xlabel('\% Active Time')

	plt.savefig('hprof.pdf')

def main(argv):
	parser = argparse.ArgumentParser()
	parser.add_argument('file')

	args = parser.parse_args(sys.argv[1:])

	d = process(args.file)
	plot_hprof(d)

if __name__ == '__main__':
	main(sys.argv)
