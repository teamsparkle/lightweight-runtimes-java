import matplotlib
matplotlib.use('Agg')	# To allow plotting over ssh
import matplotlib.pyplot as plt
from pylab import rc

rc('font',**{'family':'serif','serif':['Times'], 'size': 5})
rc('text', usetex=True)

def new_fig():
	plt.close()
	fig = plt.figure(figsize=(3.3,2.5))
	return fig

def save(ax, xlabel=None, ylabel=None, title=None, filename=None, **kwargs):
	if xlabel:
		ax.set_xlabel(xlabel)
	if ylabel:
		ax.set_ylabel(ylabel)
	if title:
		ax.set_title(title)

	prefix = kwargs.get('prefix', None)
	if prefix:
		filename = prefix + '_' + filename
	if filename:
		plt.savefig(filename, bbox_inches='tight', dpi=600)

def memory_performance(memory, performance, **kwargs):
	ax = kwargs.get('ax', None)
	if not ax:
		fig = new_fig()
		ax = fig.add_subplot(111)

	label = kwargs.get('label', None)
	ax.plot(memory, performance, label=label)

	if not kwargs.get('ax', None):
		# No ax specified..so save
		save(ax, **kwargs)

def data_scaling(scaling_dict, ax=None, filename=None, **kwargs):
	if not ax:
		fig = new_fig()
		ax = fig.add_subplot(111)

	keys = scaling_dict.keys()
	x_axis = sorted([int(x.strip('mb')) for x in keys])
	d = {}

	query = scaling_dict[keys[0]]['timeTaken'][0].keys()[0].strip('.sql').upper()
	for key in keys:
		v = int(key.strip('mb'))
		arr = scaling_dict[key]['timeTaken']
		values = [x.values()[0] for x in arr]
		d[v] = sum(values) / float(len(values))

	y_axis = [d[x] for x in x_axis]

	label = kwargs.get('label', None)
	ax.plot(x_axis, y_axis, label=label)

	if not filename:
		filename = query.lower() + '_data_scaling.jpg'
	if kwargs.get('save', None):
		print 'Saving to: %s' % (filename)
		ax.legend()
		save(ax, xlabel='Data Size (MB)', ylabel='Time Taken (s)', title='Data Scaling - ' + query, filename=filename, **kwargs)
