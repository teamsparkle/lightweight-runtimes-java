#!/usr/bin/python
import os,sys,argparse
import logging
import json
import shlex

from pycommons import generic_logging
logger = logging.getLogger()

from pycommons import pycommons

import common
import run
import find_min_heap

def compute_difference(old, new):
	return (old - new) / float(old)

def process(min_heap, query, args, repeat=1):
	current_performance = None
	multiplier = 1
	mem_perf_dict = {}
	stable_steps = 0
	while 1:
		new_heap = int(min_heap * float(multiplier))

		java_opts = '-Xms%dk -Xmx%dk' % (new_heap, new_heap)


		times = []
		for i in range(repeat):
			cmdline = run.generate_cmdline(args.args, java_opts)
			ret, stdout, stderr = run.run(cmdline, log=True, fail_on_error=False)

			if ret != 0:
				logger.error("Error detected")
				for line in stdout.split('\n'): logger.error(line)
				if stderr:
					for line in stderr.split('\n'):
						logger.error(line)
				raise Exception("Failed: \n%s\n" % (cmdline))

			new_performance = None
			for line in stdout.split('\n'):
				try:
					obj = json.loads(line)
					t = obj['timeTaken']
					# Take only the second half of time values
					for d in t[len(t)/2:]:
						times.extend(d.values())
				except:
					continue

		new_performance = sum(times) / float(len(times))
		mem_perf_dict[new_heap] = new_performance

		if not new_performance:
			logger.critical('Did not find JSON with performance values')
			for line in stdout.split('\n'):
				print line
			raise Exception()

		# The following code is for performance based termination
		if not current_performance:
			current_performance = new_performance
		else:
			diff = compute_difference(current_performance, new_performance)
			logger.info("%fx = %f" % (multiplier, diff))
			if abs(diff) < args.threshold:
				stable_steps += 1
				logger.debug('stable_steps: %d' % (stable_steps))
				if stable_steps > args.stable_steps:
					break
			else:
				stable_steps = 0
				current_performance = new_performance
		multiplier += args.step


	memory = sorted(mem_perf_dict.keys())
	performance = [mem_perf_dict[k] for k in memory]

	logger.info(json.dumps(mem_perf_dict))
	if not args.dump:
		import plot
		if args.filename:
			filename = args.filename
		else:
			filename = '%s_mem_perf.pdf' % (query)
		plot.memory_performance(memory, performance,
			xlabel='Memory (KB)', ylabel='Performance (s)',
			title='Memory vs Performance', filename=filename)

def setup_parser(parser=None):
	if not parser:
		parser = argparse.ArgumentParser()

	parser.add_argument('--queries', '-q', action=pycommons.ListAction,
			type=str, required=True, help='Queries to run')
	parser.add_argument('--step', '-s', type=float, default=0.4,
			help='Step size to increment memory')
	parser.add_argument('--threshold', '-t', type=float, default=0.03,
			help='Performance threhsold within which to terminate')
	parser.add_argument('--dump', action='store_true', default=False,
			help='Dump values instead of plotting')
	parser.add_argument('--stable-steps', '-S', type=int, default=10,
			help='Consecutive stable steps')
	parser.add_argument('--filename', '-f', action='store', default=None,
			help='Filename for plot')

	return parser

def main(argv):
	parser = common.setup_parser()
	parser = setup_parser(parser)
	args = parser.parse_args(argv[1:])

	global logger
	generic_logging.init(args.level, args.log, format=generic_logging.__FILE_FORMAT)
	logger = logging.getLogger()

	for q in args.queries:
		logger.info('------------------------------------------------------------------')
		logger.info("Processing query '%s'" % (q))
		default_args = args.args

		common.build_args(args, q, 1)
		min_heap = find_min_heap.find_min_heap(args.args)
		args.args = default_args

		common.build_args(args, q, args.repeat)
		process(min_heap, q, args, repeat=1)
		args.args = default_args
		logger.info('------------------------------------------------------------------\n\n')


if __name__ == '__main__':
	main(sys.argv)
