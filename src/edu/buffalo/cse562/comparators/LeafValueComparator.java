package edu.buffalo.cse562.comparators;

import net.sf.jsqlparser.expression.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by keno on 5/5/15.
 */
public class LeafValueComparator implements Comparator<LeafValue> {

    @Override
    public int compare(LeafValue t1, LeafValue t2) {
        try{
        if(t1 instanceof StringValue)
        {
            return (t1.toString()).compareTo(t2.toString());
        }
        if(t1 instanceof DoubleValue)
        {
            return Double.compare(t1.toDouble(), t2.toDouble());//(t1.toDouble()).compareTo(t2.toDouble());
        }
        if(t1 instanceof LongValue)
        {
            int returnVal;
            try{
                returnVal = Long.compare(t1.toLong(), t2.toLong());
            }
            catch (Exception ex){
                returnVal = Long.compare(t1.toLong(),
                        Long.parseLong(t2.toString().replace("\'", "")));
            }
            return returnVal;
        }

        if(t1 instanceof DateValue)
        {
            DateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            Date dateCurr = sf.parse(t1.toString());
            Date dateNxt = sf.parse(t2.toString());
            return dateCurr.compareTo(dateNxt);
        }
        } catch (ParseException | LeafValue.InvalidLeaf e){
            e.printStackTrace();
        }
        return 0;
    }
}
