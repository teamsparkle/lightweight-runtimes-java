package edu.buffalo.cse562.bdbindexing;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.SecondaryDatabase;
import edu.buffalo.cse562.structures.DataRow;
import edu.buffalo.cse562.util.Util;

import java.util.HashMap;

public class SimpleSecKeyCreator implements com.sleepycat.je.SecondaryKeyCreator {
    private final HashMap<Integer, String> typeMap;
    private TupleBinding<DataRow> theBinding;
	private int column;

	public SimpleSecKeyCreator(int column, HashMap<Integer, String> typeMap) {
		this.column = column;
        theBinding = new BinderWithFilter(typeMap);
        this.typeMap = typeMap;
	}

	@Override
	public boolean createSecondaryKey(SecondaryDatabase secDb,
            DatabaseEntry keyEntry, 
            DatabaseEntry dataEntry,
            DatabaseEntry resultEntry) {
        DataRow row = theBinding.entryToObject(dataEntry);
        resultEntry.setData(Util.write(row.getRow().get(column), typeMap.get(column)));
		return true;
	}
}
