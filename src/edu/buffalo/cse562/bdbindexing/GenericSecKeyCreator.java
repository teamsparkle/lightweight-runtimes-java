package edu.buffalo.cse562.bdbindexing;

import com.sleepycat.bind.tuple.TupleBinding;
import com.sleepycat.je.DatabaseEntry;
import com.sleepycat.je.SecondaryDatabase;
import edu.buffalo.cse562.structures.DataRow;
import edu.buffalo.cse562.structures.Datum;
import edu.buffalo.cse562.util.Util;

import java.util.HashMap;

public class GenericSecKeyCreator implements com.sleepycat.je.SecondaryKeyCreator {
	private TupleBinding<DataRow> theBinding;
	private int column;
	private String type;

	public GenericSecKeyCreator(TupleBinding<DataRow> secKey, int column, HashMap<Integer, String> typeMap) {
		theBinding = secKey;
		this.column = column;
		this.type = typeMap.get(column);
	}

	@Override
	public boolean createSecondaryKey(SecondaryDatabase secDb,
            DatabaseEntry keyEntry, 
            DatabaseEntry dataEntry,
            DatabaseEntry resultEntry) {		
		DataRow row = theBinding.entryToObject(dataEntry);
		Datum secIndexDatum = row.getRow().get(column);
		resultEntry.setData(Util.write(secIndexDatum, type));
		return true;
	}
}
