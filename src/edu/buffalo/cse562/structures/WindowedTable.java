package edu.buffalo.cse562.structures;

import edu.buffalo.cse562.Main;

import java.util.*;

public class WindowedTable {
	LinkedList<ArrayList<Datum>> windowedTable = new LinkedList<>();
	public LinkedList<Long> timeStamps = new LinkedList<Long>();
	String tableName = "";
    //default size initially
	public int window_records= 60*10*5;
	int currentIndexRead=0;
    String currentTuple = null;
    ListIterator<ArrayList<Datum>> listIter;
    private HashMap<Integer, String> indexMaps = null;
    private TreeMap<Integer,Integer> shrinkedIndexMap = null;
    Integer[] shrinkedIndexMapArr = null;
    private HashMap<String,ColumnDetail> operatorTableSchema = null;


    public WindowedTable(String tableName){
		this.tableName = tableName;
        shrinkedIndexMap = new TreeMap<>();

        HashMap<String,ColumnDetail> intSchema = Main.tableMapping.get(this.tableName.toLowerCase());
        this.indexMaps = Main.indexTypeMaps.get(this.tableName.toLowerCase());
        if (intSchema == null){
            intSchema = Main.tableMapping.get(this.tableName.toUpperCase());
            this.indexMaps = Main.indexTypeMaps.get(this.tableName.toUpperCase());
        }
        // old one


        // new one
        this.operatorTableSchema = this.initialiseOutputTableSchema(intSchema);
        shrinkedIndexMapArr = getShrinkedIndexArr(shrinkedIndexMap);
        reset();
	}

    private HashMap<String,ColumnDetail> initialiseOutputTableSchema(HashMap<String,ColumnDetail>  createTableSchemaMap)
    {
        HashMap<String,ColumnDetail> opT = new HashMap<String,ColumnDetail>();

        String columnName = "";
        int counter = 0;
        for(Map.Entry<String, ColumnDetail> es : createTableSchemaMap.entrySet())
        {
            String nameKey = es.getKey();

            String tableAlias = "";
            if(tableAlias != null)
            {
                if(nameKey.contains("."))
                {
                    String[] columnWholeTableName = nameKey.split("\\.");
                    nameKey = tableAlias +"."+columnWholeTableName[1];
                }
            }

            if(nameKey.contains("."))
            {
                String[] columnWholeTableName = nameKey.split("\\.");
                columnName = columnWholeTableName[1];
            }
            else
            {
                columnName = es.getKey();
            }

            if(Main.tableColumns.get(tableName.toLowerCase()).contains(columnName))
            {
                shrinkedIndexMap.put(counter, es.getValue().getIndex());
                ColumnDetail cd = es.getValue().clone();
                cd.setIndex(counter);
                opT.put(nameKey,cd);
                counter++;
            }

//			opT.put(nameKey,es.getValue().clone());
        }
        return opT;
    }

    private Integer[] getShrinkedIndexArr(TreeMap<Integer,Integer> shrinkedIndexMap)
    {
        Integer[] shrinkedIndexMapArr = new Integer[shrinkedIndexMap.size()];

        int counter = 0;
        for(Map.Entry<Integer,Integer> ind: shrinkedIndexMap.entrySet())
        {
            shrinkedIndexMapArr[counter] = ind.getValue();
            counter++;
        }

        return shrinkedIndexMapArr;

    }
	public void reset() {
		currentIndexRead=0;
		listIter = windowedTable.listIterator();
	}

	public int getIndexRead(){
		return currentIndexRead;
	}

	//clears the contents of the table
	public void clear() {
		windowedTable.clear();
		reset();
	}

	public String insert(String rowString){
        String[] col = rowString.split("\\|");
        ArrayList<Datum> tuple = getTuples(col);
		if(windowedTable.size() >= window_records)
		{
			windowedTable.poll();
		}

		windowedTable.offer(tuple);
		return "Success";
	}

	public ArrayList<Datum> readOneTuple(){
		if(currentIndexRead == windowedTable.size()) {
			reset();
			return null;
		}

		currentIndexRead++;
		//return windowedTable.get(currentIndexRead-1);
        if (listIter.hasNext()){
            return listIter.next();
        } else {
			//Ideally we should never hit here as
			//if(currentIndexRead == windowedTable.size()) handles end of list
			return null;
		}
	}

    public ArrayList<Datum> getTuples(String[] col)
    {
        ArrayList<Datum> tuples = new ArrayList<Datum>();

        for(Integer i:shrinkedIndexMapArr)
        {
            String type = indexMaps.get(i);
            tuples.add(new Datum(type, col[i]));

        }
        return tuples;
    }

	public void setWindowSize(double frequency_InsertsPerMinute,double time_window)
	{
		int temp = (int) (((frequency_InsertsPerMinute*time_window*60) * 10) / 10);
        System.out.println("temp: " + temp);
        System.out.println("freq: " + frequency_InsertsPerMinute);

		//poll the extra elements in current window Records
        if(windowedTable.size() > temp) {
            for(int i=temp;i < window_records;i++ )
                windowedTable.poll();
            reset();

        }
        window_records = temp;
	}
}
