package edu.buffalo.cse562.test;

import net.sf.jsqlparser.expression.LeafValue;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.TreeMap;

public class LightScan {
	FileChannel fc;
	String line;
	TreeMap<Integer, String> typeMap;
    ByteBuffer bf;
    private static final char ROW_DELIMITER = '\n';
    private static final char FIELD_DELIMITER = '|';
    ArrayList<String> cTuple = new ArrayList<>();
    char [] currentChar = new char[1024];
    int currentPosition = 0;
    int datumPosition = 0;
    int lastPosition = 0;
    int bufferCondition = 0;
    CharBuffer cbuff = CharBuffer.allocate(0);
    int tupleLength;

    public LightScan(String tableName, TreeMap<Integer,String> typeMap) {
		// TODO Auto-generated constructor stub
//        fc = DBManager.getInstance().getChannel(tableName);
//        bf = ByteBuffer.allocate(256);
//        this.typeMap = typeMap;
//        this.tupleLength = typeMap.size();
	}


	public ArrayList<LeafValue> readTuple(){
        System.out.println("start state: " + datumPosition + " " + currentPosition+ " " + lastPosition);
        try {
            while (bufferCondition != -1) {
                if (currentPosition >= cbuff.length()) {
                    //Save the decoded contents of the buffer in a char buffer
                    bufferCondition = fillBuffer();
                    System.out.println("Refilling buffer...");
                    if (bufferCondition == -1){
                        fc.close();
//                        System.out.println("Closing stream permanently");
                        return null;
                    }
                    bf.rewind();
                    cbuff = StandardCharsets.UTF_8.decode(bf);
                    currentPosition = 0;
                }
                for (int i = currentPosition; i <= cbuff.length(); i++) {
                    char c = cbuff.get();
//                    System.out.print(c);
                    if (c == ROW_DELIMITER) {
                        //Do nothing

                    } else if (c == FIELD_DELIMITER) {
                        System.out.println("===");
                        String type = typeMap.get(datumPosition);
//                        System.out.println(datumPosition + " - " +type);
                        if (type == null){
                            System.out.println("No entry in typemap for " +datumPosition);
                        }
                        cTuple.add(new String(currentChar, 0, lastPosition));
                        lastPosition = 0;
                        if (datumPosition >= tupleLength - 1){
                            System.out.println(cTuple);
                            cTuple = new ArrayList<>();
                            System.out.println("Found row delimiter at datum position " +datumPosition);
                            datumPosition = 0;
                            currentPosition = i;
                            System.out.println("return state: " + datumPosition + " " + currentPosition+ " " + lastPosition);
                            return null;
                        }
                        datumPosition++;
                        //Print current tuple and move on
                    } else {
                        currentChar[lastPosition] = c;
                        lastPosition++;
                        System.out.println(new String(currentChar, 0, lastPosition));
                    }
                }
                bf.clear();
                currentPosition = 0;
                bufferCondition = fillBuffer();
            }
            fc.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("return state: " + datumPosition + " " + currentPosition+ " " + lastPosition);
        return null;
    }

    private int fillBuffer(){
        try {
            return fc.read(bf);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
