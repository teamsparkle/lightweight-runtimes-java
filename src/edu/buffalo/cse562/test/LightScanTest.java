package edu.buffalo.cse562.test;

import edu.buffalo.cse562.util.ConfigManager;

import java.util.Date;
import java.util.TreeMap;

/**
 * Created by keno on 5/6/15.
 */
public class LightScanTest {
    public static void main(String[] args){
        String tableName = "customer";
        ConfigManager.setDataDir("/home/keno/code/562/200MB");
        TreeMap<Integer, String> typeMap = new TreeMap<>();
        typeMap.put(0,"INT");
        typeMap.put(1,"CHAR");
        typeMap.put(2,"CHAR");
        typeMap.put(3,"INT");
        typeMap.put(4,"CHAR");
        typeMap.put(5,"DECIMAL");
        typeMap.put(6,"CHAR");
        typeMap.put(7,"CHAR");

        LightScan lightScan = new LightScan(tableName, typeMap);
        long start = new Date().getTime();
        for (int i = 0; i < 5; i++) {
            lightScan.readTuple();
        }
        System.out.println("Executed in " + ((float) (new Date().getTime() - start)/ 1000) + "s");
    }
}
