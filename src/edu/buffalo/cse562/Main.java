package edu.buffalo.cse562;

import edu.buffalo.cse562.bdbindexing.BDBCreator;

import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import edu.buffalo.cse562.bdbindexing.DBManager;
import edu.buffalo.cse562.structures.WindowedTable;
import edu.buffalo.cse562.util.Util;
import edu.buffalo.cse562.structures.Datum;
import edu.buffalo.cse562.queryoperations.QueryOptimizer;
import edu.buffalo.cse562.operators.Operator;
import edu.buffalo.cse562.queryoperations.SanitizeQuery;
import edu.buffalo.cse562.util.ConfigManager;
import edu.buffalo.cse562.structures.ColumnDetail;
import edu.buffalo.cse562.operators.UnionOperator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.Float;
import java.lang.System;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;

import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.Union;

import org.json.simple.JSONArray;
/* To dump stuff for post processing */
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



public class Main {

	public static HashMap<String, HashMap<String, ColumnDetail>> tableMapping = new HashMap<>();
	public static HashMap<String,HashMap<Integer, String>> indexTypeMaps = new HashMap<>();
	public static HashMap<String,ArrayList<String>> tableColumns = new HashMap<>();
	public static HashMap<String,Integer> secondaryKeyCols = new HashMap<>();
	public static HashMap<String, WindowedTable> table_windowedObject_map =  new HashMap<>();
    public static HashMap<String, Operator> queryCache = new HashMap<>();

	static int queryCount = 0;
	static boolean  tpchexec = true;
    static CCJSqlParserManager ccm = new CCJSqlParserManager();
	private static int FREQ_CONSTANT = 20;
	private static boolean completeResult;

	public static void main(String[] args) {
		if(args.length < 3){
			System.out.println("Incomplete arguments");
			return;
		}
		ArrayList<String> sqlFiles = new ArrayList<>();
		boolean loadPhase = false;
		int repeat = 1;

		for (int i = 0; i<args.length; i++) {
			if (args[i].equals("--data")) {
				ConfigManager.setDataDir(args[i + 1]);
			} else if (args[i].equals("--db")) {
				ConfigManager.setDBDir(args[i + 1]);
			} else if (args[i].endsWith(".sql")) {
				sqlFiles.add(args[i]);
			} else if (args[i].equals("--load")) {
				loadPhase = true;
			}
			else if (args[i].equals("--swap")) {
				ConfigManager.setSwapDir(args[i + 1]);
			}
			else if (args[i].equals("--repeat")) {
				repeat = Integer.parseInt(args[i+1]);
			}
		}

        ArrayList<HashMap<String, Float>> timeMaps = new ArrayList<HashMap<String, Float>>();
		for (int i=0; i<repeat; i++) {
			HashMap<String, Float> queryTimes = processSchema(sqlFiles, loadPhase);
			timeMaps.add(queryTimes);
			initiateServer(7677);
		}

        try {
            JSONObject obj = new JSONObject();
            obj.put("timeTaken", timeMaps);
            System.out.println(obj.toString());
            /*
            StringWriter writer = new StringWriter();
            JSONValue.writeJSONString(timeMaps, writer);
            System.out.println(writer.toString());
            */
        } catch(Exception e) {
            System.err.println(e.getMessage());
        }
	}

	private static HashMap<String, Float> processSchema(ArrayList<String> sqlQueries, boolean loadPhase) {

		ArrayList<File> queryFiles = new ArrayList<>();
		HashMap<String, Float> queryTimes = new HashMap<>(queryFiles.size());

		for(String q : sqlQueries){
			queryFiles.add(new File(q));
		}

		for (File f : queryFiles) {
			long start = new Date().getTime();
			try {
				CCJSqlParser parser = new CCJSqlParser(new FileReader(f));
				ExecuteFile(parser, loadPhase, false);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} finally {
                DBManager.getInstance().closeAll();
            }
			float timeTaken = (float) (new Date().getTime() - start) / 1000;
			queryTimes.put(f.getName(), timeTaken);
		}
        return queryTimes;
	}

	private static void createBDB(CreateTable createTableObj) {
		/*Creates BDB database and collects stats for each table*/
		String tableName = createTableObj.getTable().getWholeTableName().toLowerCase();
		SanitizeQuery sq = new SanitizeQuery();
		List<ColumnDefinition> cds = (List<ColumnDefinition>) createTableObj.getColumnDefinitions();
		ArrayList<Integer> primaryKeyColumns = new ArrayList<>();
		for (Index ind : (List<Index>)createTableObj.getIndexes()){
			if (ind.getType().equals("PRIMARY KEY")){
				List<String> columnNames = (List<String>) ind.getColumnsNames();
				for (String cName : columnNames) {
					int keyPosition = Main.tableColumns.get(tableName).indexOf(cName.toLowerCase());
					primaryKeyColumns.add(keyPosition);
				}
			}
		}
		if (secondaryKeyCols.containsKey(tableName)) {
			int secondaryCol = secondaryKeyCols.get(tableName);
			System.out.println("Creating BDB for table " + tableName + " with primary keys on columns " +
							primaryKeyColumns + " and secondary index on " + secondaryCol);
			new BDBCreator(tableName, primaryKeyColumns, secondaryCol);
		}
		else{
			System.out.println("Creating BDB for table " + tableName + " with primary keys on columns " +
					primaryKeyColumns + " with no secondary index");
			new BDBCreator(tableName, primaryKeyColumns);
		}
	}

	/**
	 * (non javaDocs)
	 * prepares table schema information and saves it in a static hashmap
	 * @param createTableObj createTableObject from jsql parser
	 * @author Shiva
	 */
	private static void prepareTableSchema(CreateTable createTableObj){
		@SuppressWarnings("unchecked")
		String[] tableNames = new String[1];
		String tableName = createTableObj.getTable().getWholeTableName().toLowerCase();
		SanitizeQuery sq = new SanitizeQuery();
		List<ColumnDefinition> cds = (List<ColumnDefinition>) createTableObj.getColumnDefinitions();
		HashMap<String, ColumnDetail> tableSchema = new HashMap<String, ColumnDetail>();
		HashMap<Integer, String> typeInfo = new HashMap<Integer, String>();
		int colCount = 0;
		for(ColumnDefinition colDef : cds){
			ColumnDetail columnDetail = new ColumnDetail();
			columnDetail.setTableName(tableName);
			columnDetail.setColumnDefinition(colDef);
			columnDetail.setIndex(colCount);
			String colname = colDef.getColumnName().toLowerCase();
			String columnFullName = tableName + "."+ colname;

			typeInfo.put(colCount, colDef.getColDataType().getDataType()); //indexMaps : {tableName:{columnIndex:columnType}}

			tableSchema.put(columnFullName, columnDetail);
			sq.addColumnToTable(tableName, colname);
			colCount++;
		}
		tableMapping.put(tableName,tableSchema);
		indexTypeMaps.put(tableName, typeInfo);
		table_windowedObject_map.put(tableName, new WindowedTable(tableName));

		initSecondarykeyCols();
	}

	private static void initSecondarykeyCols() {
		secondaryKeyCols.put("lineitem", 0);
		secondaryKeyCols.put("orders", 1);
		secondaryKeyCols.put("nation", 1);
		secondaryKeyCols.put("customer", 6);
	}

	static void printTuple(ArrayList<Datum> singleTuple) {
		for(int i=0; i < singleTuple.size();i++){

			try
			{
				String str = (singleTuple.get(i)==null)?"":singleTuple.get(i).toString();
				System.out.print(str);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				System.err.println(singleTuple.get(i));
			}

			if(i != singleTuple.size() - 1) System.out.print("|");
		}
		System.out.println();
	}

	static String getTupleAsString(ArrayList<Datum> singleTuple) {

		StringBuilder sb = new  StringBuilder();
		for(int i=0; i < singleTuple.size();i++){

			try
			{
				String tupleStr = (singleTuple.get(i)==null)?"":singleTuple.get(i).toString();
				sb.append(tupleStr);
				//System.out.print(str);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				System.err.println(singleTuple.get(i));
			}

			if(i != singleTuple.size() - 1) sb.append("|");
		}
		sb.append("\n");
		return sb.toString();
		// System.out.println();
	}

	static String ExecuteQuery(Operator op)
	{
		ArrayList<Datum> dt=null;
		StringBuilder sb = new StringBuilder();
		do
		{
			dt = op.readOneTuple();
			if(dt !=null)
			{
//				printTuple(dt);
				sb.append(getTupleAsString(dt));
			}

		}while(dt!=null && acceptInputRow);
		
		if(dt!=null) {
			completeResult = false;
			System.out.println("Execution stopped as the runtime went over " + EXECUTE_TIME_THRESHOLD + "milliseconds");
		}
		for( Entry<String, WindowedTable> table : table_windowedObject_map.entrySet()){
			table.getValue().reset();
		}
		//System.out.println(sb.toString());
		return (sb.toString());
	}

    static void ExecuteSilent(Operator op)
    {
        ArrayList<Datum> dt=null;
        StringBuilder sb = new StringBuilder();
        do
        {
            dt = op.readOneTuple();

        }while(dt!=null);
    }


	static void printPlan(Operator op)
	{
		while(op!=null)
		{
			System.out.println(op.toString());
			op = op.getChildOp();
		}
	}

	private static  boolean writeOneToDisk(ArrayList<Datum> out){
		PrintWriter pw;
		File writeDir = getFileHandle();
		try {
			//append to file; useful for merging, and ensures that there is never a fileNotFound exception
			pw = new PrintWriter(new BufferedWriter(new FileWriter(writeDir, true)));
			Util.printToStream(out, pw);
			//util.printTuple(out);
			pw.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private static  File getFileHandle(){
		String fname = "temp.dat";
		File writeDir = new File(ConfigManager.getSwapDir(), fname);

		if (!writeDir.exists()){
			try {
				writeDir.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return writeDir;
	}


	static String readFile(String path, Charset encoding)
			throws IOException
	{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static Operator parseQuery(Statement statement){
		SanitizeQuery sq = new SanitizeQuery();
		Operator operator;
		if(statement instanceof Select){
			SelectBody select = ((Select) statement).getSelectBody();
			if (select instanceof PlainSelect){
				operator = sq.generateTree(select);
//				printPlan(op);
				new QueryOptimizer(operator);
//				printPlan(op);
				return operator;
			}
			else if (select instanceof Union){
				Union un = (Union) select;
				UnionOperator uop = new UnionOperator();
				List<PlainSelect> plainSelects = (List<PlainSelect>) un.getPlainSelects();
				for (PlainSelect s : plainSelects){
					uop.addOperator(sq.generateTree(s));
				}
				return uop;
			}
		}
		if(statement instanceof CreateTable){
			CreateTable createTableObj = (CreateTable) statement;
			prepareTableSchema(createTableObj);
		}
		return null;
	}

    public static void executeQuery(String query, JSONObject result) throws ParseException {
		InputStream is = new ByteArrayInputStream( query.getBytes() );
		InputStreamReader isr = new InputStreamReader(is);
		Operator op = null;
		if (!queryCache.containsKey(query)){
			try {
				op = parseQuery(ccm.parse(isr));
                if (queryCount >= 5) {
                    queryCache.put(query, op);
                }
                else{
                    queryCount += 1;
                }
			} catch(Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
				result.put("status", "Invalid Syntax");
			}
		}
		else {
			op = queryCache.get(query);
		}
		op.reset();
        QueryOptimizer.reorderJoins(op);
        String resultStr = ExecuteQuery(op);
        result.put("confidence", completeResult);
        result.put("response", resultStr);
        completeResult = true;

    }

	public static String ExecuteFile(CCJSqlParser parser, boolean loadPhase, boolean silent)
	{
		Statement statement;
		SanitizeQuery sq = new SanitizeQuery();
		//ExpressionTree sq = new ExpressionTree();
		try {
			while ((statement = parser.Statement()) != null){
				//					System.out.println(statement);
				if(statement instanceof Select){
					SelectBody select = ((Select) statement).getSelectBody();

					if (select instanceof PlainSelect){
						// 	System.err.println(select);
						// Operator op = e.generateTree(select);
						Operator op = sq.generateTree(select);
						try {
							long start = new Date().getTime();
							if(ConfigManager.getSwapDir() == null || ConfigManager.getSwapDir().isEmpty())
							{
								new QueryOptimizer(op);
							}
							else
							{
								// new QueryOptimizer(op);
								new QueryOptimizer(op);
							}
                        } catch(Exception ex) {
							ex.printStackTrace();
						}
					}
					else if (select instanceof Union){
						Union un = (Union) select;
						UnionOperator uop = new UnionOperator();
						List<PlainSelect> plainSelects = (List<PlainSelect>) un.getPlainSelects();
						for (PlainSelect s : plainSelects){

							uop.addOperator(sq.generateTree(s));
						}
						ExecuteQuery(uop);
					}

				}

				if(statement instanceof CreateTable){
					CreateTable createTableObj = (CreateTable) statement;
					prepareTableSchema(createTableObj);

					//if(createTableObj.getTable().getName().toLowerCase().equals("roomproperties"))testInsertRoomProp();//test and insert
					if(createTableObj.getTable().getName().toLowerCase().equals("rooms"))testInsertRoom();
					if (loadPhase) {
						createBDB(createTableObj);
					}
                }
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}


	private static void initiateServer(int portNumber ) {
		System.out.println("Now listening on " + portNumber);
		ServerSocket serverSocket = null;
		try{
			serverSocket = new ServerSocket(portNumber);

			while(true) {
				Socket socket = serverSocket.accept();
				BufferedReader request = new BufferedReader(new InputStreamReader (socket.getInputStream()));
				String message = null;
				try {
					while((message = request.readLine()) != null) {
						 String response = processRequest(message);
						 System.out.println(response);
						 sendMessage(socket, response);
					}
				} catch(Exception e) {
					System.err.println(e);
				} finally {
					continue;
				}
			}

		}catch(Exception e){
			System.out.println("Suggestion  : Check if the client request has new line in it.");
			System.out.println(e);
		}finally{
			try{
				serverSocket.close();
			}catch(Exception e){
				System.out.println("Couldn't close server socket on port" +  portNumber);
				e.printStackTrace();
			}
		}

	}


	private static void sendMessage(Socket socket, String message) throws Exception {
		int intSize = (Integer.SIZE / 8);		
		ByteBuffer buffer = ByteBuffer.allocate(intSize + message.length());
		buffer.putInt(message.length());
		buffer.put(message.getBytes("UTF-8"));
//		System.out.println("Writing " + message + "(" + (intSize + message.length()) + " bytes" + ")");
		OutputStream out = socket.getOutputStream();
		out.write(buffer.array());
		out.flush();
	}
	static boolean acceptInputRow = true;
	static long EXECUTE_TIME_THRESHOLD = 100l;
	static Runnable setStopFlag = () -> {
		try{
			Thread.sleep(EXECUTE_TIME_THRESHOLD);
			acceptInputRow = false;
		}catch(InterruptedException e){
			System.out.println("Error in setStopFlag creation of thread");
			e.printStackTrace();
		}
	};
	private static String processRequest(Object request){
		JSONObject result = new JSONObject();
        result.put("db", "sparkledb");
	    String strRequest = (String) request;
	    try{
	    JSONObject jsonObj =(JSONObject) new JSONParser().parse(strRequest);
	    String type = (String)jsonObj.get("queryType");
	    if(type.equals( "insert")){
	    	String tableName = (String)jsonObj.get("tableName");
	    	JSONArray values = (JSONArray) jsonObj.get("values");
	    	for(int i=0; i<values.size(); i++){
	    		String value = values.get(i).toString();
	    		updateTimeStamp(tableName);
		    	insertValues(tableName, value);
	    	}
	    	result.put("status", "OK");
	    }else if (type.equals("select")){
	    	String values = (String) jsonObj.get("values");
	    	double timeLimit = (Double) jsonObj.get("time_limit");
	    	
	    	acceptInputRow = true;
	    	if(timeLimit != 0) {
		    	Thread thread = new Thread(setStopFlag);
		    	thread.start();
		    	EXECUTE_TIME_THRESHOLD = (long) (timeLimit * 1000);
	    	}
            
            long startTimeStamp = System.currentTimeMillis();
	    	executeQuery(values, result);
            long timeTaken = System.currentTimeMillis() - startTimeStamp;
            result.put("time_taken_ms", timeTaken);
            if(!result.containsKey("status")){
            	result.put("status", "OK");
            }
	    }
	    else if(type.equals("resize")){
	    	String tableName = (String)jsonObj.get("tableName");
	    	double window_time_mins = (Double) jsonObj.get("window_minutes");
	    	double insertsPerSecond = getInsertsPerSecondCount(tableName);
	    	
	    	if(window_time_mins == 0) window_time_mins = 10;
	    	updateWindow(tableName,  window_time_mins,  insertsPerSecond );
	    	result.put("status", "OK");
	    }
	    }catch(Exception e){
			String message = "Possibly error in parsing the select or insert statements " + strRequest;
	    	System.err.println(message);
	    	e.printStackTrace();
	    	result.put("status", "500! " + e.getMessage());
	    }
	    return result.toJSONString();
	}
	
	private static double getInsertsPerSecondCount(String tableName) {
		WindowedTable wt = table_windowedObject_map.get(tableName.toLowerCase());
		long first = wt.timeStamps.peek();
		long last = wt.timeStamps.getLast();
		long timeForLastNInserts = last - first;
		float timeFor1InsertMs  = timeForLastNInserts /(float) wt.timeStamps.size();
		
        System.out.println("1insertMs :" + timeFor1InsertMs);
		
		return (double) 1000/timeFor1InsertMs;
	}

	private static void updateTimeStamp(String tableName) {
		WindowedTable wt = table_windowedObject_map.get(tableName.toLowerCase());
		wt.timeStamps.add(System.currentTimeMillis());
		if(wt.timeStamps.size() > FREQ_CONSTANT ){
			wt.timeStamps.poll();
		}
	}

	private static void insertValues(String tableName, String values) {
		WindowedTable wt = table_windowedObject_map.get(tableName.toLowerCase());
		wt.insert(values);
	}
	
	private static void updateWindow(String tableName,double window_time_mins, double insert_freq) {
		WindowedTable wt = table_windowedObject_map.get(tableName.toLowerCase());
		wt.setWindowSize(window_time_mins,insert_freq);
	}

	private static void testInsertRoomProp() {
		WindowedTable wt = table_windowedObject_map.get("roomproperties");
		String rowStr="";
		boolean alternate = false;

		for(int i=0;i<100;i++)
		{
			if(!alternate)
				rowStr=i+"|23|45|2|2245"+i%4;
			else
				rowStr=i+"|21|70|0|2240"+i%3;
			wt.insert(rowStr);
			alternate=!alternate;
		}
	}

	private static void testInsertRoom() {
		WindowedTable wt = table_windowedObject_map.get("rooms");
		HashMap<Integer,String> hmap = new HashMap<>();
		hmap.put(1,"Norton 101");
		hmap.put(2,"Knox 202");
		hmap.put(3,"Alumni Arena");
		hmap.put(4,"Dining Room");
		hmap.put(5,"C3");
		hmap.put(6,"Davis 101");
		hmap.put(7,"Davis 329");
		hmap.put(8,"Davis 234");
		hmap.put(9,"Capen 202");
		hmap.put(10,"Gym");
		hmap.put(11,"Judo Hall");
		String rowStr="";

		for(int i=1;i<12;i++)
		{				
			rowStr=i+"|"+hmap.get(i);
			wt.insert(rowStr);
		}
	}

}
