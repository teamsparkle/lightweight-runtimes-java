package edu.buffalo.cse562.util;

/**
 * Created by keno on 5/5/15.
 */
public enum SearchMode {
    G, GEQ, M, MEQ, EQ, SCAN;
}
