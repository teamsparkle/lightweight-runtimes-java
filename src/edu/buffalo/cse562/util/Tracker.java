package edu.buffalo.cse562.util;

import java.util.HashMap;

/**
 * Created by kenob on 11/18/15.
 */
public class Tracker {
    static Tracker _currentInstance = null;
    HashMap<String, Integer> frequencyMap;

    public Tracker(){
        this.frequencyMap = new HashMap<>();
    }

    public static Tracker getInstance(){
        if (_currentInstance == null){
            _currentInstance = new Tracker();
        }
        return _currentInstance;
    }

    public void updateInsertFrequency(String tableName, int value){
        this.frequencyMap.put(tableName, value);
    }

    public int getInsertFrequency(String tableName){
        // Just for testing, the second statement is what would eventually hold
        return 100 - Math.abs(tableName.hashCode() % 100);
//        return this.frequencyMap.getOrDefault(tableName, 0);
    }
}
