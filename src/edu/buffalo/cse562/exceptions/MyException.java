/**
 * 
 */
package edu.buffalo.cse562.exceptions;

/**
 * @author Sathish
 *
 */
public class MyException extends Exception {
    public MyException(String message) {
        super(message);
    }
}