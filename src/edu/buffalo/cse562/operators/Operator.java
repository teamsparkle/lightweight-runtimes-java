/**
 * 
 */
package edu.buffalo.cse562.operators;

import edu.buffalo.cse562.structures.ColumnDetail;
import edu.buffalo.cse562.structures.Datum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author Sathish
 *
 */
public interface Operator {
	
	/**
	 * returns one tuple at a time
	 * @return
	 */
	ArrayList<Datum> readOneTuple();
	
	/**
	 * Returns the output tuple schema the implementer may produce
	 * @return
	 */
	HashMap<String,ColumnDetail> getOutputTupleSchema();
	
	/**
	 * resets the iterator to the initial item
	 */
	void reset();
	
	/**
	 * Returns its child operator
	 * @return
	 */
	Operator getChildOp();
		
	/***
	 * Sets the child Operator 
	 */
	void setChildOp(Operator child);
	
	/**
	 * Gets the parent Operator
	 * @param parent
	 */
	Operator getParent();
	
	/**
	 * Sets the parent Operator
	 * @param parent
	 */
	void setParent(Operator parent);

    HashSet<String> getUsedColumns();

	int getSize();

	List<Operator> getAllChildren();

	int getWindowSize();
}
